package com.tezzt.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping("/user")
	public Long addUser(@RequestBody User user) {
		return userService.add(user).getId();
	}

//    @PostMapping("/login")
//    public String login(@RequestBody AppUser user) {
//        return appUserService.login(user);
//    }
}
