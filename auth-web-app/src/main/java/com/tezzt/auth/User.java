package com.tezzt.auth;

import lombok.*;
import javax.persistence.*;



@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String username;
    private String role; 
}
