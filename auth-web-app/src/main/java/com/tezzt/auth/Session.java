package com.tezzt.auth;

import lombok.*;

import javax.persistence.*;
import javax.validation.Constraint;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="SessionId")
    private Integer id;

    private String token;
    private String username;

    private Date creationDate;
}
