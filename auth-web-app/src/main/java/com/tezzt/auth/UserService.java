package com.tezzt.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserService  {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;

    public User add(User user){
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

//    public String login(AppUser user) {
//        if(appUserRepository.findByUsername(user.getUsername()).size() > 0) {
//            if(appUserRepository.findByUsername(user.getUsername()).get(0).getPassword() == encoder.encode(user.getPassword())) {
//                String token =
//            }
//
//    }
}
